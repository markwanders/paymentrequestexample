var date = new Date();
date.setDate(date.getDate() + 7);
document.getElementById('validUntil').value = date.toISOString().replace("Z", "+01:00");

showcase = function () {
    var xmlHttpRequest = initiateHttpRequest();

    xmlHttpRequest.open("GET", "/greeting", true);
    xmlHttpRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlHttpRequest.send();

};

registerMerchant = function () {
  merchantRequest("POST")
};

updateMerchant = function () {
    merchantRequest("PATCH")
};

merchantRequest = function(verb) {
    var request = {};
    request.merchantId = document.getElementById('merchantId').value;
    request.merchantSubId = document.getElementById('merchantSubId').value;
    request.merchantName = document.getElementById('merchantName').value;
    request.merchantIBAN = document.getElementById('merchantIBAN').value;
    request.merchantLogo = document.getElementById('merchantLogo').value;
    request.dailyReceivableLimit = {};
    request.dailyReceivableLimit.value = document.getElementById('dailyReceivableLimit').value;
    request.dailyReceivableLimit.currency = "EUR";
    request.allowIngAppPayments = document.querySelector('input[name="allowIngAppPayments"]:checked').value;

    var requestJSON = JSON.stringify(request, undefined, 2);

    var xmlHttpRequest = initiateHttpRequest();

    xmlHttpRequest.open(verb, "/registrations/", true);
    xmlHttpRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlHttpRequest.send(requestJSON);
}

createPaymentRequest = function () {
    var request = {};
    request.description = document.getElementById('description').value;
    request.purchaseId = document.getElementById('purchaseId').value;
    var variable = document.getElementById('variable').checked;
    if (variable) {
        request.variableAmount = {};
        request.variableAmount.currency = "EUR";
        request.variableAmount.suggestedValue = document.getElementById('amount').value;
        request.variableAmount.minimumValue = 0.01;
        request.variableAmount.maximumValue = 5000;
    } else {
        request.fixedAmount = {};
        request.fixedAmount.value = document.getElementById('amount').value;
        request.fixedAmount.currency = "EUR";
    }
    request.returnUrl = document.getElementById('returnUrl').value;
    if (document.getElementById('maximumReceivableAmount').value) {
        request.maximumReceivableAmount = {};
        request.maximumReceivableAmount.value = document.getElementById('maximumReceivableAmount').value;
        request.maximumReceivableAmount.currency = "EUR";
        if (variable) {
            request.variableAmount.maximumValue = request.maximumReceivableAmount.value;
        }
    }
    request.maximumAllowedPayments = document.getElementById('purchaseId').maximumAllowedPayments;
    request.validUntil = document.getElementById('validUntil').value;

    var requestJSON = JSON.stringify(request, undefined, 2);

    var xmlHttpRequest = initiateHttpRequest();

    xmlHttpRequest.open("POST", "/payment-requests/", true);
    xmlHttpRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlHttpRequest.send(requestJSON);
};

getPaymentRequest = function () {
    var id = document.getElementById('id').value;

    var xmlHttpRequest = initiateHttpRequest();

    xmlHttpRequest.open("GET", "/payment-requests/" + id, true);
    xmlHttpRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlHttpRequest.send();
};

getPayments = function () {
    var id = document.getElementById('id').value;

    var xmlHttpRequest = initiateHttpRequest();

    xmlHttpRequest.open("GET", "/payment-requests/" + id + "/payments", true);
    xmlHttpRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlHttpRequest.send();
};

closePaymentRequest = function () {
    var id = document.getElementById('id').value;

    var xmlHttpRequest = initiateHttpRequest();

    xmlHttpRequest.open("POST", "/payment-requests/" + id + "/close", true);
    xmlHttpRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlHttpRequest.send();
};

getRegistration = function () {
    var xmlHttpRequest = initiateHttpRequest();

    xmlHttpRequest.open("GET", "/registrations/", true);
    xmlHttpRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlHttpRequest.send();
};

initiateHttpRequest = function () {
    var xmlHttpRequest = new XMLHttpRequest();
    xmlHttpRequest.onreadystatechange = function () {
        if (xmlHttpRequest.readyState === 4) {
            if (xmlHttpRequest.responseText) {
                if (xmlHttpRequest.getResponseHeader("content-type").toLowerCase().includes("json")) {
                    var responseJSON = JSON.parse(xmlHttpRequest.responseText);
                    if (responseJSON.hasOwnProperty("paymentInitiationUrl") && responseJSON.hasOwnProperty("id")) {
                        document.getElementById("qr").src = "/payment-requests/qr/" + responseJSON.id + ".png";
                        document.getElementById("qr").classList.remove("d-none");
                    }
                    document.getElementById("modal-body").innerHTML = JSON.stringify(responseJSON, null, 2);
                } else {
                    document.getElementById("modal-body").innerHTML = xmlHttpRequest.responseText
                }
            } else {
                document.getElementById("modal-body").innerHTML = "Status " + xmlHttpRequest.status + " " + xmlHttpRequest.statusText
            }
        }
    };

    return xmlHttpRequest;
};
