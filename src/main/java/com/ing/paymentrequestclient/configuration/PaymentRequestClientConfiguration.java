package com.ing.paymentrequestclient.configuration;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.Assert;
import org.springframework.util.ResourceUtils;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.text.DateFormat;

@Configuration
public class PaymentRequestClientConfiguration {
    @Value("${http.signing-key-alias}")
    private String signingAlias;

    @Value("${http.signing-key-store}")
    private String signingKeyStore;

    @Value("${http.signing-key-store-password}")
    private String signingKeyStorePassword;

    @Value("${server.ssl.key-store}")
    private String sslKeyStore;

    @Value("${server.ssl.key-store-password}")
    private String sslKeyStorePassword;

    @Bean
    public Certificate sslCertificate() {
        try (InputStream keyStoreInputStream = Thread.currentThread().getContextClassLoader().
                getResourceAsStream(sslKeyStore)) {
            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            char[] keyStorePassword = sslKeyStorePassword.toCharArray();
            keyStore.load(keyStoreInputStream, keyStorePassword);

            return keyStore.getCertificate(signingAlias);
        } catch (IOException | CertificateException | NoSuchAlgorithmException | KeyStoreException e) {
            throw new IllegalArgumentException("Failed to load certificate: ", e);
        }
    }

    @Bean
    public PrivateKey privateKey() {
        try (InputStream keyStoreInputStream = Thread.currentThread().getContextClassLoader().
                getResourceAsStream(signingKeyStore)) {
            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            char[] keyStorePassword = signingKeyStorePassword.toCharArray();
            keyStore.load(keyStoreInputStream, keyStorePassword);

            //Fetch our private key we use for signing
            Key key = keyStore.getKey(signingAlias, keyStorePassword);
            PrivateKey privateKey = (PrivateKey) key;
            Assert.notNull(privateKey, "Private key not found for alias " + signingAlias);

            return privateKey;

        } catch (IOException | CertificateException | NoSuchAlgorithmException | UnrecoverableKeyException | KeyStoreException e) {
            throw new IllegalArgumentException("Failed to load public and private keys: ", e);
        }
    }

    @Bean
    public Signature signature() throws NoSuchAlgorithmException {
        return Signature.getInstance("SHA256withRSA");
    }

    @Bean
    public RestTemplate mutualTLSRestTemplate(RestTemplateBuilder builder) throws Exception {
        char[] password = sslKeyStorePassword.toCharArray();

        KeyStore keyStore = KeyStore.getInstance("PKCS12");
        File key = ResourceUtils.getFile(sslKeyStore);
        try (InputStream in = new FileInputStream(key)) {
            keyStore.load(in, password);
        }

        SSLContext sslContext = SSLContextBuilder.create()
                .loadKeyMaterial(keyStore, password)
                .loadTrustMaterial(null, new TrustSelfSignedStrategy()).build();

        HttpClient client = HttpClients.custom().useSystemProperties().setSSLContext(sslContext).build();
        return builder
                .requestFactory(() -> new HttpComponentsClientHttpRequestFactory(client))
                .build();
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper()
                .setSerializationInclusion(JsonInclude.Include.NON_EMPTY)
                .registerModule(new JavaTimeModule())
                .setDateFormat(DateFormat.getDateInstance())
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    }
}
