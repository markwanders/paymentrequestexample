package com.ing.paymentrequestclient.controller;

import com.google.zxing.WriterException;
import com.ing.paymentrequest.CreatePaymentRequest;
import com.ing.paymentrequest.CreatePaymentRequestResponse;
import com.ing.paymentrequest.GetPaymentRequestResponse;
import com.ing.paymentrequest.GetPaymentsResponse;
import com.ing.paymentrequestclient.client.PaymentRequestClient;
import com.ing.paymentrequestclient.service.QRService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@AllArgsConstructor
@Slf4j
public class PaymentRequestController {
    private final PaymentRequestClient paymentRequestClient;
    private final QRService qrService;

    @GetMapping(path = "/payment-requests/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GetPaymentRequestResponse> get(@PathVariable String id) {
        return paymentRequestClient.getPaymentRequest(id);
    }

    @GetMapping(path = "/payment-requests/{id}/payments", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GetPaymentsResponse> getPayments(@PathVariable String id) {
        return paymentRequestClient.getPayments(id);
    }

    @PostMapping(path = "/payment-requests/", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CreatePaymentRequestResponse> create(@RequestBody CreatePaymentRequest createPaymentRequest) {
        return paymentRequestClient.createPaymentRequest(createPaymentRequest);
    }

    @PostMapping(path = "/payment-requests/{id}/close")
    public ResponseEntity<String> close(@PathVariable String id) {
        return paymentRequestClient.closePaymentRequest(id);
    }

    @GetMapping(path = "/payment-requests/qr/{id}.png", produces = MediaType.IMAGE_PNG_VALUE)
    public ResponseEntity<byte[]> qr(@PathVariable final String id) throws IOException, WriterException {
        GetPaymentRequestResponse paymentRequestResponse = paymentRequestClient.getPaymentRequest(id).getBody();
        Assert.notNull(paymentRequestResponse, "PaymentRequest is null");
        return ResponseEntity.ok(qrService.generateQRCode(paymentRequestResponse.getPaymentInitiationUrl()));
    }

}
