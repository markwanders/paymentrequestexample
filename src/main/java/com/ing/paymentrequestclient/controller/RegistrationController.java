package com.ing.paymentrequestclient.controller;

import com.ing.paymentrequest.GetRegistrationResponse;
import com.ing.paymentrequest.RegistrationRequest;
import com.ing.paymentrequest.UpdateRegistrationRequest;
import com.ing.paymentrequestclient.client.PaymentRequestClient;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@Slf4j
public class RegistrationController {
    private final PaymentRequestClient paymentRequestClient;


    @PostMapping(path = "/registrations/", produces = MediaType.TEXT_PLAIN_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> register(@RequestBody RegistrationRequest registrationRequest) {
        log.info("register {}", registrationRequest);
        return paymentRequestClient.registerMerchant(registrationRequest);
    }

    @GetMapping(path = "/registrations/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GetRegistrationResponse> getRegistration() {
        return paymentRequestClient.getRegistration();
    }

    @PatchMapping(path = "/registrations/", produces = MediaType.TEXT_PLAIN_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> update(@RequestBody UpdateRegistrationRequest updateRegistrationRequest) {
        log.info("update registration {}", updateRegistrationRequest);
        return paymentRequestClient.updateRegistration(updateRegistrationRequest);
    }
}
