package com.ing.paymentrequestclient.controller;

import com.ing.paymentrequestclient.client.ShowcaseClient;
import com.ing.showcase.Greeting;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@Slf4j
public class ShowcaseController {
    private final ShowcaseClient showcaseClient;

    @RequestMapping(path = "/greeting", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Greeting> get() {
        log.info("get greeting");
        return showcaseClient.get();
    }
}
