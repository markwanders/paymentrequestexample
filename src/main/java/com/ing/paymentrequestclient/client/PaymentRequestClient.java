package com.ing.paymentrequestclient.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ing.paymentrequest.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Objects;

@Service
@Slf4j
@AllArgsConstructor
public class PaymentRequestClient {
    private static final String ING_URL = "https://api.ing.com";

    private final RestTemplate mutualTLSRestTemplate;

    private final ObjectMapper objectMapper;

    private final ClientUtils clientUtils;

    private final AccessTokenClient accessTokenClient;

    public ResponseEntity<String> registerMerchant(RegistrationRequest registrationRequest) {
        String path = "/payment-requests/registrations";
        try {
            ResponseEntity<String> responseEntity = doPost(path, registrationRequest);

            return ResponseEntity.ok(Objects.requireNonNull(responseEntity.getBody()));
        } catch (HttpClientErrorException e) {
            log.error("Caught HttpClientErrorException while registering merchant:{} {} {}", e.getStatusCode(), e.getStatusText(), e.getResponseBodyAsString());
            return ResponseEntity.status(e.getStatusCode()).build();
        } catch (IOException e) {
            log.error("Caught IOException while registering merchant", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity<GetRegistrationResponse> getRegistration() {
        String path = "/payment-requests/registrations";
        String requestURL = ING_URL + path;
        try {
            HttpHeaders headers = clientUtils.createHeaders(path, "get", "", MediaType.APPLICATION_JSON, accessTokenClient.getAccessToken());

            HttpEntity<String> request = new HttpEntity<>(headers);

            ResponseEntity<String> responseEntity = mutualTLSRestTemplate.exchange(
                    requestURL,
                    HttpMethod.GET,
                    request,
                    String.class);

            return ResponseEntity.ok(objectMapper.readValue(responseEntity.getBody(), GetRegistrationResponse.class));
        } catch (HttpClientErrorException e) {
            log.error("Caught HttpClientErrorException while getting merchant registration: {} {} {}", e.getStatusCode(), e.getStatusText(), e.getResponseBodyAsString());
            return ResponseEntity.status(e.getStatusCode()).build();
        } catch (IOException e) {
            log.error("Caught IOException while registering merchant", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity<String> updateRegistration(UpdateRegistrationRequest updateRegistrationRequest) {
        String path = "/payment-requests/registrations";
        try {
            ResponseEntity<String> responseEntity = doPatch(path, updateRegistrationRequest);

            return ResponseEntity.status(responseEntity.getStatusCode()).build();
        } catch (HttpClientErrorException e) {
            log.error("Caught HttpClientErrorException while updating merchant: {} {} {}", e.getStatusCode(), e.getStatusText(), e.getResponseBodyAsString());
            return ResponseEntity.status(e.getStatusCode()).build();
        } catch (IOException e) {
            log.error("Caught IOException while updating merchant", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity<GetPaymentRequestResponse> getPaymentRequest(String id) {
        String path = "/payment-requests/" + id;
        String requestURL = ING_URL + path;

        try {
            HttpHeaders headers = clientUtils.createHeaders(path, "get", "", MediaType.APPLICATION_JSON, accessTokenClient.getAccessToken());

            HttpEntity<String> request = new HttpEntity<>(headers);

            ResponseEntity<String> responseEntity = mutualTLSRestTemplate.exchange(
                    requestURL,
                    HttpMethod.GET,
                    request,
                    String.class);

            return ResponseEntity.ok(objectMapper.readValue(responseEntity.getBody(), GetPaymentRequestResponse.class));
        } catch (HttpClientErrorException e) {
            log.error("Caught HttpClientErrorException while getting payment request: {} {} {}", e.getStatusCode(), e.getStatusText(), e.getResponseBodyAsString());
            return ResponseEntity.status(e.getStatusCode()).build();
        } catch (IOException e) {
            log.error("Caught IOException while getting payment request", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity<GetPaymentsResponse> getPayments(String id) {
        String path = "/payment-requests/" + id + "/payments";
        String requestURL = ING_URL + path;

        try {
            HttpHeaders headers = clientUtils.createHeaders(path, "get", "", MediaType.APPLICATION_JSON, accessTokenClient.getAccessToken());

            HttpEntity<String> request = new HttpEntity<>(headers);

            ResponseEntity<String> responseEntity = mutualTLSRestTemplate.exchange(
                    requestURL,
                    HttpMethod.GET,
                    request,
                    String.class);

            return ResponseEntity.ok(objectMapper.readValue(responseEntity.getBody(), GetPaymentsResponse.class));
        } catch (HttpClientErrorException e) {
            log.error("Caught HttpClientErrorException while getting payment request payments: {} {} {}", e.getStatusCode(), e.getStatusText(), e.getResponseBodyAsString());
            return ResponseEntity.status(e.getStatusCode()).build();
        } catch (IOException e) {
            log.error("Caught IOException while getting payment request payments", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity<CreatePaymentRequestResponse> createPaymentRequest(CreatePaymentRequest createPaymentRequest) {
        String path = "/payment-requests";
        try {
            ResponseEntity<String> responseEntity = doPost(path, createPaymentRequest);

            return ResponseEntity.ok(objectMapper.readValue(responseEntity.getBody(), CreatePaymentRequestResponse.class));
        } catch (HttpClientErrorException e) {
            log.error("Caught HttpClientErrorException while creating payment request: {} {} {}", e.getStatusCode(), e.getStatusText(), e.getResponseBodyAsString());
            return ResponseEntity.status(e.getStatusCode()).build();
        } catch (IOException e) {
            log.error("Caught IOException while creating payment request", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity<String> closePaymentRequest(String id) {
        String path = "/payment-requests/" + id + "/close";
        String requestURL = ING_URL + path;

        try {
            String body = "";

            HttpHeaders headers = clientUtils.createHeaders(path, "post", body, MediaType.APPLICATION_JSON, accessTokenClient.getAccessToken());

            HttpEntity<String> request = new HttpEntity<>(body, headers);

            return mutualTLSRestTemplate.exchange(
                    requestURL,
                    HttpMethod.POST,
                    request,
                    String.class);
        } catch (HttpClientErrorException e) {
            log.error("Caught HttpClientErrorException while closing payment request: {} {} {}", e.getStatusCode(), e.getStatusText(), e.getResponseBodyAsString());
            return ResponseEntity.status(e.getStatusCode()).build();
        }
    }

    private ResponseEntity<String> doPost(String path, Object request) throws JsonProcessingException {
        String requestURL = ING_URL + path;
        String body = objectMapper.writeValueAsString(request);

        log.debug("Sending post to {} with body {}", requestURL, body);

        HttpHeaders headers = clientUtils.createHeaders(path, "post", body, MediaType.APPLICATION_JSON, accessTokenClient.getAccessToken());

        HttpEntity<String> httpEntity = new HttpEntity<>(body, headers);

        ResponseEntity<String> responseEntity = mutualTLSRestTemplate.exchange(
                requestURL,
                HttpMethod.POST,
                httpEntity,
                String.class);

        log.info("Received response: {}", responseEntity);

        return responseEntity;
    }

    private ResponseEntity<String> doPatch(String path, Object request) throws JsonProcessingException {
        String requestURL = ING_URL + path;
        String body = objectMapper.writeValueAsString(request);

        log.debug("Sending patch to {} with body {}", requestURL, body);

        HttpHeaders headers = clientUtils.createHeaders(path, "patch", body, MediaType.APPLICATION_JSON, accessTokenClient.getAccessToken());

        HttpEntity<String> httpEntity = new HttpEntity<>(body, headers);

        ResponseEntity<String> responseEntity = mutualTLSRestTemplate.exchange(
                requestURL,
                HttpMethod.PATCH,
                httpEntity,
                String.class);

        log.info("Received response: {}", responseEntity);

        return responseEntity;
    }
}
