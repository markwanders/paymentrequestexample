package com.ing.paymentrequestclient.client;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import java.security.InvalidKeyException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.springframework.http.MediaType.*;

@Slf4j
@Service
public class ClientUtils {
    private final String clientId;
    private final Signature signature;
    private final PrivateKey privateKey;
    static final String ING_URL = "https://api.ing.com";

    public ClientUtils(Signature signature, PrivateKey privateKey, @Value("${client-id}") String clientId) {
        this.signature = signature;
        this.privateKey = privateKey;
        this.clientId = clientId;
    }

    HttpHeaders createHeaders(String path, String method, String body, MediaType mediaType, String accessToken) {
        String reqId = UUID.randomUUID().toString();
        String date = getServerTime();
        String digest = "SHA-256=" + new String(Base64.getEncoder().encode(DigestUtils.sha256(body)));
        String stringToSign = "(request-target): " + method + " " + path + "\n" +
                "date: " + date + "\n" +
                "digest: " + digest + "\n" +
                "x-ing-reqid: " + reqId;
        String signature = "keyId=\"" + clientId + "\",algorithm=\"rsa-sha256\",headers=\"(request-target) date digest x-ing-reqid\",signature=\"" + signBase64(stringToSign) + "\"";

        HttpHeaders headers = new HttpHeaders();
        if (!"/oauth2/token".equals(path)) {
            headers.set("Authorization", "Bearer " + accessToken);
            headers.set("Signature", signature);
        } else {
            headers.set("authorization", "Signature " + signature);
        }
        headers.set("X-ING-ReqID", reqId);
        headers.set("Date", date);
        headers.set("Digest", digest);
        headers.setContentType(mediaType);
        headers.setAccept(Arrays.asList(APPLICATION_JSON, TEXT_PLAIN));

        return headers;
    }

    private String signBase64(String message) {
        try {
            signature.initSign(privateKey);
            signature.update(message.getBytes());
            byte[] signatureBytes = signature.sign();

            return Base64.getEncoder().encodeToString(signatureBytes);
        } catch (SignatureException | InvalidKeyException e) {
            throw new RuntimeException("Failed to sign string: ", e);
        }
    }

    private static String getServerTime() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return dateFormat.format(calendar.getTime());
    }
}
