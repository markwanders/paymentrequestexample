package com.ing.paymentrequestclient.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ing.showcase.Greeting;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

import static com.ing.paymentrequestclient.client.ClientUtils.ING_URL;

@Service
@Slf4j
@AllArgsConstructor
public class ShowcaseClient {
    private final RestTemplate mutualTLSRestTemplate;

    private final ObjectMapper objectMapper;

    private final ClientUtils clientUtils;

    private final AccessTokenClient accessTokenClient;

    public ResponseEntity<Greeting> get() {
        String path = "/greetings/single";
        String requestURL = ING_URL + path;

        try {
            HttpHeaders headers = clientUtils.createHeaders(path, "get", "", MediaType.APPLICATION_JSON, accessTokenClient.getAccessToken());

            HttpEntity<String> request = new HttpEntity<>(headers);

            ResponseEntity<String> responseEntity = mutualTLSRestTemplate.exchange(
                    requestURL,
                    HttpMethod.GET,
                    request,
                    String.class);

            return ResponseEntity.ok(objectMapper.readValue(responseEntity.getBody(), Greeting.class));
        } catch (HttpClientErrorException e) {
            log.error("Caught HttpClientErrorException while getting greeting: {} {} {}", e.getStatusCode(), e.getStatusText(), e.getResponseBodyAsString());
            return ResponseEntity.status(e.getStatusCode()).build();
        } catch (IOException e) {
            log.error("Caught IOException while getting greeting", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}
