package com.ing.paymentrequestclient.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ing.paymentrequest.AccessTokenResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;

import static com.ing.paymentrequestclient.client.ClientUtils.ING_URL;

@Slf4j
@AllArgsConstructor
@Service
public class AccessTokenClient {
    private final ObjectMapper objectMapper;

    private final RestTemplate mutualTLSRestTemplate;

    private final ClientUtils clientUtils;

    String getAccessToken() {
        String body = "grant_type=client_credentials";

        String path = "/oauth2/token";
        String requestURL = ING_URL + path;

        try {
            HttpHeaders headers = clientUtils.createHeaders(path, "post", body, MediaType.APPLICATION_FORM_URLENCODED, null);

            HttpEntity<String> request = new HttpEntity<>(body, headers);

            ResponseEntity<String> responseEntity = mutualTLSRestTemplate.exchange(
                    requestURL,
                    HttpMethod.POST,
                    request,
                    String.class);

            String accessToken = objectMapper
                    .readValue(responseEntity.getBody(), AccessTokenResponse.class).getAccessToken();

            log.info("Received access token {}", accessToken);

            return accessToken;
        } catch (HttpClientErrorException e) {
            log.error("Caught HttpClientErrorException while getting access token: {} {} {}", e.getStatusCode(), e.getStatusText(), e.getResponseBodyAsString());
            throw new ResponseStatusException(e.getStatusCode(), e.getMessage());
        } catch (IOException e) {
            log.error("Caught IOException while getting access token", e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }
}
