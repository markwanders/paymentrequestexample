package com.ing.paymentrequestclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaymentRequestClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(PaymentRequestClientApplication.class, args);
    }
}
