# PaymentRequestExample

Example client for the ING Developer Portal [PaymentRequestAPI](https://developer.ing.com/api-marketplace/marketplace/01199464-4247-4770-8ab8-c3371052e9e5/overview).


To generate the appropriate certificates/keystores:
```
openssl genrsa -out server.key -passout stdin 2048
openssl req -sha256 -new -key server.key -out server.csr
openssl x509 -req -sha256 -days 365 -in server.csr -signkey server.key -out server_public.crt
openssl req -sha256 -new -key server.key -out tls.csr
openssl x509 -req -sha256 -days 365 -in tls.csr -signkey server.key -out tls_public.crt
openssl pkcs12 -export -out tls.p12 -inkey server.key -in tls_public.crt 
openssl pkcs12 -export -out server.p12 -inkey server.key -in server_public.crt 
```

Upload the public certificates to your ING developer dashboard, tls_public to Mutual TLS, and server_public to HTTPS Request Signing.
Then, move your server.p12 and tls.p12 to the resources directory and set your own client-id in the application.properties.


Start this application and open https://localhost:8080 in Google Chrome.
Try typing 'thisisunsafe' if you see a message that the connection is not private.